<?php

/* View */

include_once("header.php");
include_once('class/Login.class.php');

$objLogin = new Login();
switch ($objLogin->setAction) {

    case "":
        ?><div class="container">
    <h1> Login Page </h1>
    <div class="row h-100 justify-content-center align-items-center">

        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" onsubmit="javascript: return jsValidation(this);">
            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" name="userEmail" class="form-control" id="email">
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" name="userPass" class="form-control" id="pwd">
            </div>
            <input type="hidden" name="setAction" value="login">
            <button type="submit" class="btn btn-default">Login</button>
            <a href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . "?setAction=register"; ?>">Register here...</a>
        </form>
    </div>
    <?php
        if (isset($objLogin->errors) && !empty($objLogin->errors)) {
            ?>
    <div class="alert alert-danger">
        <?php
                print($objLogin->errors);
                ?></div>
    <?php
        }
        ?>
</div>
<script>
    function jsValidation(objForm) {
        if (trimfield(objForm.userEmail.value) == '') {
            alert("Name Required!");
            objForm.userEmail.focus()
            return false;
        }
        if (!validateEmail(trimfield(objForm.userEmail.value))) {
            alert("Valid Email Required!");
            objForm.userEmail.focus()
            return false;
        }
        if (trimfield(objForm.userPass.value) == '') {
            alert("Valid Password Required!");
            objForm.userPass.focus()
            return false;
        }
        return true;
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function trimfield(str) {
        return str.replace(/^\s+|\s+$/g, '');
    }
</script><?php
                break;


            case "register":
                ?><div class="container">
    <h1> Register Page </h1>
    <div class="row h-100 justify-content-center align-items-center">

        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" onsubmit="javascript: return jsValidation(this);">
            <table>

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>

                <div class="form-group">
                    <label for="code">Code:</label>
                    <input type="text" name="code" class="form-control" id="code">
                </div>

                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" name="price" class="form-control" id="price">
                </div>

                <input type="hidden" name="setAction" value="reg">
                <button type="submit" class="btn btn-default">Register</button>
        </form>
    </div><?php
                if (isset($objLogin->errors) && !empty($objLogin->errors)) {
                    ?><div class="alert alert-danger"><?php
                        print($objLogin->errors);
                        ?></div><?php
                                    }
                                    ?>
</div>
<script>
    function jsValidation(objForm) {

        if (trimfield(objForm.name.value) == '') {
            alert("Product Name is Required!");
            objForm.name.focus()
            return false;
        }

        if (trimfield(objForm.code.value) == '') {
            alert("Product Code Name Required!");
            objForm.code.focus()
            return false;
        }

        if (trimfield(objForm.price.value) == '') {
            alert("Product Price Required!");
            objForm.price.focus()
            return false;
        }
        return true;
    }



    function trimfield(str) {
        return str.replace(/^\s+|\s+$/g, '');
    }
</script>

<?php
        // Switch Close
}
?>

<?php
include_once "footer.php";
?>