<?php

/* Model */

require_once __DIR__ . '/../config/config.php';
class DBResult
{

    private $connection;

    function __construct()
    {

        $this->connection = mysqli_connect(HOST, DB_USERNAME, DB_PASSWORD, DATABASE);
    }

    function __destruct()
    {
        mysqli_close($this->connection);
    }


    /**
     * Create or return a connection to the MySQL server.
     */

    public function getConnection()
    {
        return $this->connection;
    }

    /*  
        Function to check user is valid or not
        @param mixed $params
        @return boolean 
    */
    public function checkUserLogin($params)
    {
        $userNAME = $this->cleanInput($params['userEmail']);
        $userPass = $this->cleanInput($params['userPass']);

        $loginQuery = "SELECT * FROM users 
                       WHERE email ='" . $userNAME . "'  AND password ='" . $userPass . "'";
        $result = mysqli_query($this->connection, $loginQuery);

        if (isset($result->num_rows) && $result->num_rows > 0) {
            return mysqli_fetch_array($result, MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /*
        Function to insert values to registerPage
        @param mixed $params
        @return bool
    */
    /*public function productRegister($reg)
    {
        $product = $this->cleanInput($reg['product']);
        $name = $this->cleanInput($reg['pName']);
        $price = $this->cleanInput($reg['pPrice']);

        $insertQuery = "INSERT INTO products (product, product_name, product_price) 
                        VALUES ('" . $product . "' , '" . $name . "' , '" . $price . "')";
        $result = mysqli_query($this->connection, $insertQuery);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }*/
    
    public function productRegister($reg)
   {
       $name = $this->cleanInput($reg['name']);
       $code = $this->cleanInput($reg['code']);
       $price = $this->cleanInput($reg['price']);
       $insertQuery = "INSERT INTO tblproduct (name, code, price)
                       VALUES ('" . $name . "' , '" . $code . "' , '" . $price . "')" ;
       $result = mysqli_query($this->connection, $insertQuery);
       if($result){
           return true;
        }
        else {
            return false;
        }
    }
    function cleanInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
