<?php

include_once("header.php");
include_once('class/Login.class.php');

$objLogin = new Login();
?><div class="container">
    <h1> Register Page </h1>
    <div class="row h-100 justify-content-center align-items-center">

        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" onsubmit="javascript: return jsValidation(this);">
            <table>
                <div class="form-group">
                    <label for="">Email address:</label>
                    <input type="email" name="userEmail" class="form-control" id="email">
                </div>

                <div class="form-group">
                    <label for="dob">DOB:</label>
                    <input type="Date" name="dob" class="form-control" id="dob">
                </div>

                <div class="form-group">
                    <label>Gender:</label><br>
                    <label for="male">Male:</label>
                    <input type="radio" name="gender" value="m" class="form-control" id="male">
                    <label for="female">Female:</label>
                    <input type="radio" name="gender" value="f" class="form-control" id="female">
                </div>

                <div class="form-group">
                    <label>Hobbies:</label><br>
                    <label for="dancing">Dancing:</label>
                    <input type="checkbox" name="hob[]" value="d" class="form-control" id="dancing">
                    <label for="singing">Singing:</label>
                    <input type="checkbox" name="hob[]" value="s" class="form-control" id="singing">
                </div>

                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" name="userPass" class="form-control" id="pwd">
                </div>
                <input type="hidden" name="setAction" value="register">
                <button type="submit" class="btn btn-default">Register</button>
        </form>
    </div><?php
            if (isset($objRegister->errors) && !empty($objRegister->errors)) {
                ?><div class="alert alert-danger"><?php
                    print($objRegister->errors);
                    ?></div><?php
                    }
                    ?>
</div>
<script>
    function jsValidation(objForm) {
        if (trimfield(objForm.userEmail.value) == '') {
            alert("Name Required!");
            objForm.userEmail.focus()
            return false;
        }
        if (!validateEmail(trimfield(objForm.userEmail.value))) {
            alert("Valid Email Required!");
            objForm.userEmail.focus()
            return false;
        }
        if (objForm.dob.value == '') {
            alert("validate DOB Required!");
            return false;
        }

        if (objForm.gender.value == '') {
            alert("validate Gender Required!");
            return false;
        }

        if (trimfield(objForm.userPass.value) == '') {
            alert("Valid Password Required!");
            objForm.userPass.focus()
            return false;
        }
        return true;
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function trimfield(str) {
        return str.replace(/^\s+|\s+$/g, '');
    }
</script><?php
include_once "footer.php";
?>