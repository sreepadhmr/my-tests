<?php

include_once("header.php");
include_once('class/Login.class.php');

$objLogin = new Login();
switch($objLogin->setAction) {

case "":
    ?><div class="container">
        <h1> Home Page </h1>
        <?php
            $arrData = $objLogin->dbObj->userDetails();
            foreach ( $arrData as $key => $value ){ 
                ?> <div class="alert alert-primary" role="alert"><?php print $key . " :- "  . $value;  ?></div><?php
            }   
        ?>
        <?php
            if(isset($objLogin->errors) && !empty($objLogin->errors)){
                ?><div class="alert alert-danger"><?php
                    print($objLogin->errors);
                ?></div><?php 
            }
        ?>

    <!-- <button type="submit" class="btn btn-default">Update</button> -->

    </div><?php
    break;

  // Switch Close
}
?>

<?php
include_once "footer.php";
?>